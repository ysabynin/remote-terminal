import {AppComponent} from './app.component';
import {ButtonModule} from 'primeng/primeng';
import {TerminalService} from 'primeng/components/terminal/terminalservice';
import {AppRoutingModule} from './routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ButtonModule
  ],
  providers: [TerminalService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
